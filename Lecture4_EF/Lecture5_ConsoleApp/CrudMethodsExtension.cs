﻿using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_ConsoleApp.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_ConsoleApp
{
    public static class CrudMethodsExtension
    {
        private static ProjectsHttpService _projectsService = new ProjectsHttpService();
        private static TasksHttpService _tasksService = new TasksHttpService();
        private static TeamsHttpService _teamsService = new TeamsHttpService();
        private static UsersHttpService _usersService = new UsersHttpService();

        // GET 

        public static void GetAllProjects()
        {
            Console.Clear();
            var result = _projectsService.GetAllProjects().GetAwaiter().GetResult();
            foreach (var item in result)
            {
                Console.WriteLine($"Project ID: {item.Id}\nProject Name: {item.Name}\nProject Description: {item.Description}\nProject Deadline: {item.Deadline}\nProject Created At: {item.CreatedAt}\n\n\n");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetAllTasks()
        {
            Console.Clear();
            var result = _tasksService.GetAllTasks().GetAwaiter().GetResult();
            foreach (var item in result)
            {
                Console.WriteLine($"Task ID: {item.Id}\nTask Name: {item.Name}\nTask Description: {item.Description}\nTask State: {item.State}\nTask Created At: {item.CreatedAt}\nTask Finished At: {item.FinishedAt}\n\n\n");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetAllTeams()
        {
            Console.Clear();
            var result = _teamsService.GetAllTeams().GetAwaiter().GetResult();
            foreach (var item in result)
            {
                Console.WriteLine($"Team ID: {item.Id}\nTeam Name: {item.Name}\nTask Created At: {item.CreatedAt}\n\n\n");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetAllUsers()
        {
            Console.Clear();
            var result = _usersService.GetAllUsers().GetAwaiter().GetResult();
            foreach (var item in result)
            {
                Console.WriteLine($"User ID: {item.Id}\nUser Name: {item.FirstName} {item.LastName}\nUser email: {item.Email}\nUser birthday: {item.BirthDay}\nUser registered At: {item.RegisteredAt}\n\n\n");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetProjectById()
        {
            Console.Clear();
            Console.WriteLine("Write project ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var item = _projectsService.GetProjectById(ID).GetAwaiter().GetResult();
            Console.WriteLine($"Project ID: {item.Id}\nProject Name: {item.Name}\nProject Description: {item.Description}\nProject Deadline: {item.Deadline}\nProject Created At: {item.CreatedAt}\n\n\n");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }
        public static void GetTaskById()
        {
            Console.Clear();
            Console.WriteLine("Write task ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var item = _tasksService.GetTaskById(ID).GetAwaiter().GetResult();
            Console.WriteLine($"Task ID: {item.Id}\nTask Name: {item.Name}\nTask Description: {item.Description}\nTask State: {item.State}\nTask Created At: {item.CreatedAt}\nTask Finished At: {item.FinishedAt}\n\n\n");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }
        public static void GetTeamById()
        {
            Console.Clear();
            Console.WriteLine("Write team ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var item = _teamsService.GetTeamById(ID).GetAwaiter().GetResult();
            Console.WriteLine($"Team ID: {item.Id}\nTeam Name: {item.Name}\nTask Created At: {item.CreatedAt}\n\n\n");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }
        public static void GetUserById()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var item = _usersService.GetUserById(ID).GetAwaiter().GetResult();
            Console.WriteLine($"User ID: {item.Id}\nUser Name: {item.FirstName} {item.LastName}\nUser email: {item.Email}\nUser birthday: {item.BirthDay}\nUser registered At: {item.RegisteredAt}\n\n\n");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }


        // DELETE

        public static void DeleteProject()
        {
            Console.Clear();
            Console.WriteLine("Write project ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var response = _projectsService.DeleteProject(ID).GetAwaiter().GetResult();
            Console.WriteLine($"Status code: {response.StatusCode}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void DeleteTask()
        {
            Console.Clear();
            Console.WriteLine("Write task ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var response = _tasksService.DeleteTask(ID).GetAwaiter().GetResult();
            Console.WriteLine($"Status code: {response.StatusCode}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void DeleteTeam()
        {
            Console.Clear();
            Console.WriteLine("Write team ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var response = _teamsService.DeleteTeam(ID).GetAwaiter().GetResult();
            Console.WriteLine($"Status code: {response.StatusCode}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void DeleteUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int ID);
            var response = _usersService.DeleteUser(ID).GetAwaiter().GetResult();
            Console.WriteLine($"Status code: {response.StatusCode}");
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        // UPDATE


        // CREATE
    }
}
