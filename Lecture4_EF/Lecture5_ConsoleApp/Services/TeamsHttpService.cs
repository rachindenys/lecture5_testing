﻿using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Team;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_ConsoleApp.Services
{
    class TeamsHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public TeamsHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/Teams/");
        }
        public async Task<List<TeamDTO>> GetAllTeams()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<TeamDTO>>(content);
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<TeamDTO>(content);
        }

        public async Task<HttpResponseMessage> DeleteTeam(int id)
        {
            return await _httpClient.DeleteAsync($"{id}");
        }

        public async Task<HttpResponseMessage> UpdateTeam(TeamDTO item)
        {
            var content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8);
            return await _httpClient.PutAsync("", content);
        }
    }
}
