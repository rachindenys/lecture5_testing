﻿using Lecture5_Common.DTO;
using Lecture5_Common.DTO.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_ConsoleApp.Services
{
    class UsersHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();

        public UsersHttpService()
        {
            _httpClient.BaseAddress = new Uri("http://localhost:5000/api/Users/");
        }
        public async Task<List<UserDTO>> GetAllUsers()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<UserDTO>>(content);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<UserDTO>(content);
        }

        public async Task<HttpResponseMessage> DeleteUser(int id)
        {
            return await _httpClient.DeleteAsync($"{id}");
        }

        public async Task<HttpResponseMessage> UpdateUser(UserDTO item)
        {
            var content = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8);
            return await _httpClient.PutAsync("", content);
        }
    }
}
