﻿using AutoMapper;
using FakeItEasy;
using Lecture5_BLL.Services;
using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Lecture5_BLL_Tests
{
    public class UserServiceTests : IDisposable
    {
        readonly AcademyDbContext context;
        readonly DbContextOptions<AcademyDbContext> options;
        readonly UserService userService;
        readonly IMapper mapper;

        public UserServiceTests()
        {
            options = new DbContextOptionsBuilder<AcademyDbContext>()
                            .UseInMemoryDatabase("UserServiceTestDB")
                            .Options;
            context = new AcademyDbContext(options);
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Project, ProjectDTO>().ReverseMap();
                cfg.CreateMap<Task, TaskDTO>().ReverseMap();
                cfg.CreateMap<User, UserDTO>().ReverseMap();
                cfg.CreateMap<Team, TeamDTO>().ReverseMap();
                cfg.CreateMap<TaskState, TaskStateDTO>().ReverseMap();
            });
            mapper = config.CreateMapper();
            userService = new UserService(context, mapper);
        }

        public void Dispose()
        {
            context.Database.EnsureDeleted();
        }

        [Fact]
        public async void Create_WhenNewUser_ThenUsersPlusOne()
        {
            var user = new User { Id = 1 };
            var fakeContext = A.Fake<AcademyDbContext>();
            var fakeDbSet = A.Fake<DbSet<User>>();
            var userService = new UserService(fakeContext, mapper);
            A.CallTo(() => fakeContext.Users).Returns(fakeDbSet);
            var newUser = await userService.Create(mapper.Map<UserDTO>(user));
            A.CallTo(() => fakeDbSet.Add(A<User>.Ignored)).MustHaveHappened();
        }

        [Fact]
        public async void Update_WhenAddedToTeam_ThenTeamSizePlusOne()
        {
            var users = new List<User>
            {
                new User { Id = 1 },
                new User { Id = 2, TeamId = 1},
                new User { Id = 3, TeamId = 1}
            };

            var teams = new List<Team>
            {
                new Team { Id = 1},
                new Team { Id = 2 }
            };
            context.AddRange(users);
            context.AddRange(teams);
            context.SaveChanges();
            var userDto = new UserDTO { Id = 1, TeamId = 1 };
            await userService.Update(userDto);
            var updatedTeam = context.Teams.Find(1);
            Assert.Equal(3, updatedTeam.Users.Count);
        }


    }
}
