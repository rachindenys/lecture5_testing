﻿using AutoMapper;
using Lecture5_BLL.Exceptions;
using Lecture5_BLL.Services.Abstract;
using Lecture5_Common.DTO;
using Lecture5_Common.DTO.Team;
using Lecture5_DAL;
using Lecture5_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_BLL.Services
{
    public sealed class TeamService : BaseService<Team>
    {
        public TeamService(AcademyDbContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<TeamDTO> Create(TeamDTO teamDTO)
        {
            var team = _mapper.Map<Team>(teamDTO);
            _context.Teams.Add(team);
            await _context.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(team);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            var team = await _context.Teams.FindAsync(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public ICollection<TeamDTO> Get()
        {
            return _mapper.Map<ICollection<TeamDTO>>(_context.Teams);
        }

        public TeamDTO Get(int id)
        {
            var team = _context.Teams.Find(id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }
            return _mapper.Map<TeamDTO>(team);
        }

        public async System.Threading.Tasks.Task Update(TeamDTO teamDTO)
        {
            var team = await _context.Teams.FindAsync(teamDTO.Id);
            if (team == null)
            {
                throw new NotFoundException(nameof(Team), teamDTO.Id);
            }
            team.CreatedAt = teamDTO.CreatedAt;
            team.Name = teamDTO.Name;
            //team.Users = _mapper.Map<List<User>>(teamDTO.Users);
            _context.Teams.Update(team);
            await _context.SaveChangesAsync();
        }
    }
}
