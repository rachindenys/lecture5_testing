﻿using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture5_Common.DTO
{
    public class UserAnalyzeDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int TotalTasksCount { get; set; }
        public int TotalUncompletedAndCanceledTasks { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
