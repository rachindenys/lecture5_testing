﻿using Lecture5_Common.DTO.Project;
using Lecture5_Common.DTO.Task;
using Lecture5_Common.DTO.Team;
using Lecture5_Common.DTO.User;
using Lecture5_DAL;
using Lecture5_WebAPI.Controllers;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Lecture5_WebAPI.IntegrationTests
{
    public class WebApiTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup>
            _factory;

        public WebApiTests(
            CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                BaseAddress = new Uri($@"http://localhost:500/api/"),
                AllowAutoRedirect = false
            });
        }

        public void Dispose()
        {
            using (var scope = _factory.Services.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<AcademyDbContext>();
                db.Database.EnsureDeleted();
                Utilities.ReinitializeDbForTests(db);
            }
        }

        [Fact]
        public async Task CreateProject_WhenNewProjectCreating_ThenNewProjectCreatedAndStatusCode201()
        {
            var projectDto = new ProjectDTO() { Id = 4, AuthorId = 3, Description = "SomeString1", Name = "GoodString", TeamId = 1, Deadline = new DateTime(2020, 1, 2), CreatedAt = new DateTime(2018, 1, 2) };
            var httpContent = new StringContent(JsonConvert.SerializeObject(projectDto), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PostAsync("Projects/", httpContent);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            var projectInfo = JsonConvert.DeserializeObject<ProjectDTO>(await httpResponse.Content.ReadAsStringAsync());
            Assert.Equal(4, projectInfo.Id);
            Assert.Equal("GoodString", projectInfo.Name);
        }

        [Fact]
        public async Task CreateProject_WhenNewProjectWithTheExistingId_ThenThrowInternalServerErrorAndProjectNotAdd()
        {
            var httpGetResponse = JsonConvert.DeserializeObject<List<ProjectDTO>>(await _client.GetStringAsync("Projects/"));
            var projectDto = new ProjectDTO() { Id = 3, AuthorId = 3, Description = "SomeString1", Name = "GoodString", TeamId = 1, Deadline = new DateTime(2020, 1, 2), CreatedAt = new DateTime(2018, 1, 2) };
            var httpContent = new StringContent(JsonConvert.SerializeObject(projectDto), Encoding.UTF8, "application/json");
            var httpPostResponse = await _client.PostAsync("Projects/", httpContent);

            Assert.Equal(HttpStatusCode.InternalServerError, httpPostResponse.StatusCode);
            Assert.Equal(3, httpGetResponse.Count);
        }

        [Fact]
        public async Task DeleteUser_WhenUserExists_ThenUserDeletedAndStatusCode204()
        {
            var userId = 2;
            var httpResponse = await _client.DeleteAsync($"Users/{userId}");
            var httpGetResponse = JsonConvert.DeserializeObject<List<UserDTO>>(await _client.GetStringAsync("Users/"));

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
            Assert.Equal(2, httpGetResponse.Count);
            Assert.True(httpGetResponse.FirstOrDefault(u => u.Id == userId) == null);
        }

        [Fact]
        public async Task DeleteUser_WhenUserDoesNotExists_ThenThrowNotFoundAndUserNotDeleted()
        {
            var userId = 4;
            var httpResponse = await _client.DeleteAsync($"Users/{userId}");
            var httpGetResponse = JsonConvert.DeserializeObject<List<UserDTO>>(await _client.GetStringAsync("Users/"));

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.Equal(3, httpGetResponse.Count);
        }

        [Fact]
        public async Task CreateTeam_WhenNewTeamCreating_ThenNewTeamCreatedAndStatusCode201()
        {
            var teamDto = new TeamDTO() { Id = 4, Name = "GoodString"};
            var httpContent = new StringContent(JsonConvert.SerializeObject(teamDto), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PostAsync("Teams/", httpContent);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            var teamInfo = JsonConvert.DeserializeObject<TeamDTO>(await httpResponse.Content.ReadAsStringAsync());
            Assert.Equal(4, teamDto.Id);
            Assert.Equal("GoodString", teamDto.Name);
        }

        [Fact]
        public async Task CreateTeam_WhenNewTeamWithTheExistingId_ThenThrowInternalServerErrorAndTeamNotAdd()
        {
            var httpGetResponse = JsonConvert.DeserializeObject<List<TeamDTO>>(await _client.GetStringAsync("Teams/"));
            var teamDto = new TeamDTO() { Id = 3 };
            var httpContent = new StringContent(JsonConvert.SerializeObject(teamDto), Encoding.UTF8, "application/json");
            var httpPostResponse = await _client.PostAsync("Teams/", httpContent);

            Assert.Equal(HttpStatusCode.InternalServerError, httpPostResponse.StatusCode);
            Assert.Equal(3, httpGetResponse.Count);
        }

        [Fact]
        public async Task CreateUser_WhenNewUserCreating_ThenNewUserCreatedAndStatusCode201()
        {
            var userDto = new UserDTO() { Id = 4, FirstName = "GoodString"};
            var httpContent = new StringContent(JsonConvert.SerializeObject(userDto), Encoding.UTF8, "application/json");
            var httpResponse = await _client.PostAsync("Users/", httpContent);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            var userInfo = JsonConvert.DeserializeObject<UserDTO>(await httpResponse.Content.ReadAsStringAsync());
            Assert.Equal(4, userInfo.Id);
            Assert.Equal("GoodString", userInfo.FirstName);
        }

        [Fact]
        public async Task CreateUser_WhenNewUserWithTheExistingId_ThenThrowInternalServerErrorAndUserNotAdd()
        {
            var httpGetResponse = JsonConvert.DeserializeObject<List<UserDTO>>(await _client.GetStringAsync("Users/"));
            var userDto = new UserDTO() { Id = 3, FirstName = "GoodString" };
            var httpContent = new StringContent(JsonConvert.SerializeObject(userDto), Encoding.UTF8, "application/json");
            var httpPostResponse = await _client.PostAsync("Users/", httpContent);

            Assert.Equal(HttpStatusCode.InternalServerError, httpPostResponse.StatusCode);
            Assert.Equal(3, httpGetResponse.Count);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskExists_ThenTaskDeletedAndStatusCode204()
        {
            var taskId = 2;
            var httpResponse = await _client.DeleteAsync($"Tasks/{taskId}");
            var httpGetResponse = JsonConvert.DeserializeObject<List<TaskDTO>>(await _client.GetStringAsync("Tasks/"));

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
            Assert.Equal(5, httpGetResponse.Count);
            Assert.True(httpGetResponse.FirstOrDefault(u => u.Id == taskId) == null);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskDoesNotExists_ThenThrowNotFoundAndTaskNotDeleted()
        {
            var taskId = 7;
            var httpResponse = await _client.DeleteAsync($"Tasks/{taskId}");
            var httpGetResponse = JsonConvert.DeserializeObject<List<TaskDTO>>(await _client.GetStringAsync("Tasks/"));

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
            Assert.Equal(6, httpGetResponse.Count);
        }

        [Fact]
        public async Task GetUncompletedTasks_WhenUserDoesNotExists_ThenThrowNotFound()
        {
            var userId = 7;
            var httpResponse = await _client.GetAsync($"Linq/8/{userId}");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task GetUncompletedTasks_WhenCorrectData_ThenReturnAllUncompletedTasksForUserAndStatusCode200()
        {
            var userId = 1;
            var httpResponse = await _client.GetAsync($"Linq/8/{userId}");
            var content = JsonConvert.DeserializeObject<List<TaskDTO>>(await httpResponse.Content.ReadAsStringAsync());
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            foreach(var item in content)
            {
                Assert.True(item.State != TaskStateDTO.Finished && item.PerformerId == userId);
            }
        }

    }
}
