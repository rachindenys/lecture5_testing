﻿using Lecture5_DAL;
using Lecture5_DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture5_WebAPI.IntegrationTests
{
    public static class Utilities
    {
        public static void InitializeDbForTests(AcademyDbContext db)
        {
            db.Projects.AddRange(GetSeedingProjects());
            db.Users.AddRange(GetSeedingUsers());
            db.Tasks.AddRange(GetSeedingTasks());
            db.Teams.AddRange(GetSeedingTeams());
            db.SaveChanges();
        }

        public static void ReinitializeDbForTests(AcademyDbContext db)
        {
            db.Projects.RemoveRange(db.Projects);
            db.Users.RemoveRange(db.Users);
            db.Tasks.RemoveRange(db.Tasks);
            db.Teams.RemoveRange(db.Teams);
            InitializeDbForTests(db);
        }

        public static List<Project> GetSeedingProjects()
        {
            return new List<Project>()
            {
                new Project(){ Id = 1, AuthorId = 1, Description = "SomeString2", Name = "SomeString3", TeamId = 3, Deadline = new DateTime(2018, 1, 2), CreatedAt = new DateTime(2016, 1, 2) },
                new Project(){ Id = 2, AuthorId = 2, Description = "SomeString3", Name = "SomeString2", TeamId = 2, Deadline = new DateTime(2019, 1, 2), CreatedAt = new DateTime(2017, 1, 2) },
                new Project(){ Id = 3, AuthorId = 3, Description = "SomeString1", Name = "SomeString1", TeamId = 1, Deadline = new DateTime(2020, 1, 2), CreatedAt = new DateTime(2018, 1, 2) }
            };
        }

        public static List<Lecture5_DAL.Entities.Task> GetSeedingTasks()
        {
            return new List<Lecture5_DAL.Entities.Task>()
            {
                new Lecture5_DAL.Entities.Task(){ Id = 1, Description = "SomeString1", Name = "SomeString3", PerformerId = 3, ProjectId = 1, State = TaskState.Cancelled, CreatedAt = new DateTime(2017, 1, 1), FinishedAt = null},
                new Lecture5_DAL.Entities.Task(){ Id = 2, Description = "SomeString11", Name = "SomeString333", PerformerId = 2, ProjectId = 2, State = TaskState.Created, CreatedAt = new DateTime(2016, 1, 1), FinishedAt = null},
                new Lecture5_DAL.Entities.Task(){ Id = 3, Description = "SomeString111", Name = "SomeString3333", PerformerId = 1, ProjectId = 3, State = TaskState.Finished, CreatedAt = new DateTime(2015, 1, 1), FinishedAt = new DateTime(2020,1,1)},
                new Lecture5_DAL.Entities.Task(){ Id = 4, Description = "SomeString1111", Name = "SomeString3333", PerformerId = 3, ProjectId = 1, State = TaskState.Finished, CreatedAt = new DateTime(2014, 1, 1), FinishedAt = new DateTime(2019,1,1)},
                new Lecture5_DAL.Entities.Task(){ Id = 5, Description = "SomeString11111", Name = "SomeString333333", PerformerId = 2, ProjectId = 2, State = TaskState.Started, CreatedAt = new DateTime(2013, 1, 1), FinishedAt = null},
                new Lecture5_DAL.Entities.Task(){ Id = 6, Description = "SomeString111111", Name = "SomeString3333333", PerformerId = 1, ProjectId = 3, State = TaskState.Created, CreatedAt = new DateTime(2012, 1, 1), FinishedAt = null}
            };
        }

        public static List<User> GetSeedingUsers()
        {
            return new List<User>()
            {
                new User(){ Id = 1, TeamId = null, FirstName = "SomeString1", LastName = "SomeString3", Email = "SomeString1@mail.ru", BirthDay = new DateTime(2010, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) },
                new User(){ Id = 2, TeamId = 1, FirstName = "SomeString2", LastName = "SomeString2", Email = "SomeString2@mail.ru", BirthDay = new DateTime(2010, 1, 1), RegisteredAt = new DateTime(2012, 1, 1) },
                new User(){ Id = 3, TeamId = 1, FirstName = "SomeString3", LastName = "SomeString1", Email = "SomeString3@mail.ru", BirthDay = new DateTime(2009, 1, 1), RegisteredAt = new DateTime(2015, 1, 1) }
            };
        }

        public static List<Team> GetSeedingTeams()
        {
            return new List<Team>()
            {
                new Team(){ Id = 1, Name = "SomeString1", CreatedAt = new DateTime(2005, 1, 1)},
                new Team(){ Id = 2, Name = "SomeString2", CreatedAt = new DateTime(2004, 1, 1)},
                new Team(){ Id = 3, Name = "SomeString3", CreatedAt = new DateTime(2003, 1, 1)}
            };
        }
    }
}
