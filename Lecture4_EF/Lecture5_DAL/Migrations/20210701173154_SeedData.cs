﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Lecture5_DAL.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 0, new DateTime(2019, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Denesik - Greenfelder" },
                    { 1, new DateTime(2017, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Durgan Group" },
                    { 2, new DateTime(2019, 2, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kassulke LLC" },
                    { 3, new DateTime(2018, 8, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harris LLC" },
                    { 4, new DateTime(2019, 4, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mitchell Inc" },
                    { 5, new DateTime(2016, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Smitham Group" },
                    { 6, new DateTime(2016, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Kutch - Roberts" },
                    { 7, new DateTime(2016, 7, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Parisian Group" },
                    { 8, new DateTime(2020, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Schiller Group" },
                    { 9, new DateTime(2018, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Littel, Turcotte and Mulle" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 2, new DateTime(2007, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Brandy.Witting@gmail.com", "Brandy", "Witting", new DateTime(2019, 1, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 3, new DateTime(2015, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Theresa82@hotmail.com", "Theresa", "Ebert", new DateTime(2017, 5, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 2, new DateTime(2020, 8, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 9, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Et doloribus et temporibus.", "backing up Handcrafted Fresh Shoes challenge", 1 },
                    { 0, 3, new DateTime(2019, 7, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 8, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Repellendus expedita dolorum saepe ut culpa nobis sunt itaque labore.", "open architecture Outdoors, Grocery & Baby Dynamic", 2 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 0, new DateTime(1953, 12, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Vivian99@yahoo.com", "Vivian", "Mertz", new DateTime(2018, 10, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 1, new DateTime(2014, 9, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Theresa_Gottlieb66@yahoo.com", "Theresa", "Gottlieb", new DateTime(2019, 12, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 3, 0, new DateTime(2020, 3, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 7, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Quia et tempora hic pariatur voluptatem doloribus sunt.", "Dam", 3 },
                    { 2, 1, new DateTime(2021, 1, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2021, 7, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Non voluptatem voluptas libero.", "Village", 0 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 0, new DateTime(2017, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eveniet nihil asperiores esse minima.", null, "index", 3, 1, 3 },
                    { 4, new DateTime(2018, 10, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Delectus quibusdam id quia iure neque maiores molestias sed aut.", null, "withdrawal contextually-based", 3, 1, 1 },
                    { 8, new DateTime(2020, 3, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aliquam laboriosam consequatur qui.", null, "Intelligent Granite Mouse", 3, 1, 0 },
                    { 3, new DateTime(2017, 8, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sint voluptatem quas.", new DateTime(2020, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "bypass", 0, 0, 2 },
                    { 7, new DateTime(2017, 10, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eveniet nihil asperiores esse minima.", null, "index", 0, 0, 3 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 2, new DateTime(2019, 2, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eum a eum.", null, "product Direct utilize", 1, 3, 0 },
                    { 6, new DateTime(2019, 2, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Aut tenetur voluptas quasi esse.", new DateTime(2020, 12, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Auto Loan Account Cambridgeshire", 1, 3, 2 },
                    { 1, new DateTime(2020, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Quo sint aut et ea voluptatem omnis ut.", null, "real-time", 2, 2, 3 },
                    { 5, new DateTime(2018, 6, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.", null, "mobile Organized", 2, 2, 1 },
                    { 9, new DateTime(2019, 8, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "Optio eum aut sunt cum nam.", null, "online", 2, 2, 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 0);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
